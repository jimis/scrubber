use std::error::Error;
use std::ffi::OsString;
use std::path::PathBuf;

#[allow(unused_imports)]
use log::{error,warn,info,debug,trace,log_enabled};             // macro
use directories::ProjectDirs;
use env_logger;
use structopt::StructOpt;


// After parsing command line, environment, and config files, this struct
// should contain configuration, and it should be read-only.
pub struct Config
{
    pub program_name    : String,
    pub argv0           : String,
    pub target_path     : PathBuf,
    pub config_file     : Option<String>,
    pub output_filename : Option<String>,
    pub env_data_dir    : Option<PathBuf>,
    pub data_dir        : PathBuf,
    pub db_file         : PathBuf,
}


#[derive(StructOpt, Debug)]
#[structopt(about="A file corruption detector")]
struct Opt
{
    /// Verbosity (-v, -vv, -vvv)
    #[structopt(short, long, parse(from_occurrences))]
    verbose : usize,

    /// Print timestamp in log messages
    #[structopt(short, long)]
    timestamp : bool,

    // TODO maybe "write a new sqlitedb file (WARNING: deletes if existing)
    /// Open a specific sqlitedb file
    #[structopt(short, long, parse(from_os_str))]
    output_db : Option<PathBuf>,

    /// Path to scan recursively
    #[structopt(parse(from_os_str))]
    target_path : PathBuf,
}


impl Config
{
    pub fn new(progname: &str, args: &mut Vec<OsString>)
        -> Result<Config, Box<dyn Error>>
    {
        let argv0 = args.remove(0).into_string()
            .expect("argv[0] (most likely executable name) is not valid unicode!");

        let opt = Opt::from_args();

        initialize_env_logger(progname, opt.verbose, opt.timestamp);

        // SCRUBBER_DATA_DIR environment variable
        let data_dir_varname = progname.to_uppercase() + "_DATA_DIR";
        let data_dir_osstring : Option<OsString> =
            std::env::var_os(&data_dir_varname);
        let env_data_dir     : Option<PathBuf> =
            data_dir_osstring.map(|s| PathBuf::from(s));
        let data_dir = match &env_data_dir {
            Some(d) => {
                trace!("Data directory found from environment variable {}: {}",
                       data_dir_varname, d.display());
                d.clone() },
            None    => ProjectDirs::from("", "", "scrubber")
                .expect("failed to figure out standard local data directory")
                .data_local_dir().to_owned()
        };
        trace!("Data directory is {}", data_dir.display());
        let db_file = match opt.output_db {
            None    => data_dir.join("hashes.sqlite"),
            Some(f) => f,
        };

        return Ok(Config {
            program_name: progname.to_string(),
            argv0       : argv0,
            target_path : opt.target_path,
            config_file     : None,
            output_filename : None,
            env_data_dir : env_data_dir,
            data_dir     : data_dir,
            db_file      : db_file,
        })
    }
}


fn initialize_env_logger(program_name: &str,
                         verbosity: usize,
                         print_timestamps: bool)
{

    // Log levels ERROR and WARN are always enabled
    let level = match verbosity {
        0 => log::LevelFilter::Warn,
        1 => log::LevelFilter::Info,
        2 => log::LevelFilter::Debug,
        _ => log::LevelFilter::Trace,
    };

    let mut builder = env_logger::Builder::new();
    builder.filter_level(level);
    if ! print_timestamps
    {
        builder.format_timestamp(None);
    }

    // SCRUBBER_LOGGER
    let logger_varname = program_name.to_uppercase() + "_LOGGER";
    if let Some(value) = std::env::var_os(logger_varname)
    {
        builder.parse_filters(value.to_str()
            .expect("LOGGER env var was not valid unicode!"));
    }
    builder.init();
}
