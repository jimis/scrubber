use super::FileData;
use super::running_state;
use running_state::RunningState;
use running_state::DevInfo;

use std::collections::HashMap;
use std::error::Error;
use std::ffi::OsString;
use std::iter::IntoIterator;
use std::path::Path;
use std::time::SystemTime;

use rusqlite;
use rusqlite::{params,NO_PARAMS};
use rusqlite::OptionalExtension;
use rusqlite::types::Null;
use serde::Serialize;
use serde_json;

#[allow(unused_imports)]
use log::{error,warn,info,debug,trace,log_enabled};             // macro


// List of tables with "id" column in sqlite db
const TABLES_WITH_ID : [&str; 5] =
    [ "previous_ids", "hashes", "filenames", "trees", "filesystems" ];

pub struct Db
{
    dbconn  : rusqlite::Connection,
    // global incrementing counter to use as a primary key for all db entries
    last_id : i64,
    written_last_id : bool,
}

// Struct to carry the results of lookup_file()
pub struct DbIds
{
    // The fs_id is public so that the caller read it and updates state struct
    pub fs_id   : Option<i64>,
    filename_id : Option<i64>,
    tree_id     : Option<i64>,
    hash_id     : Option<i64>,
    // If all the above are non-None, then check if all ids were found related
    pub final_relation_found : bool,
    // If no relation was found, then look for different hash_id for same file
    pub different_hash_id    : Option<i64>,
}

#[derive(Serialize)]
struct DevIdentifyingInfo<'a>
{
    // the drive_ prefixed fields refer to the "parent" device if it exists,
    // e.g. /dev/sda for a filesystem on /dev/sda1
    pub drive_size  : u64,
    pub drive_vendor: &'a str,
    pub drive_model : &'a str,
    pub drive_serial: &'a str,
    pub drive_wwn   : &'a str,
}

trait Utf8Warn
{
    fn utf8_warn(&self) -> &str;
}

impl Utf8Warn for OsString
{
    fn utf8_warn(&self) -> &str
    {
        self.to_str().unwrap_or_else( ||
        {
            warn!("Field from lsblk is not valid UTF8, \
                   storing empty string in db instead");
            ""
        })
    }
}


/*
NOTES

In Sqlite "INTEGER PRIMARY KEY" columns also play the role of a ROWID column.
They are automatically enforced to be UNIQUE and NOT NULL, meaning that if you
INSERT a row without `id`, it will get the next available value automatically!

In this program we always use NO ROWID tables, with our own INTEGER PRIMARY
KEY "id" column and we manually specify our "id" values in every INSERT. These
ids are always increasing and unique, even across different tables.
*/


// TODO log all INSERTs etc modifications to the db, maybe with trace!().

impl Db
{
    pub fn new<P: AsRef<Path>>(db_path: P) -> Result<Db, Box<dyn Error>>
    {
        // 1. Open database connection
        let dbconn = rusqlite::Connection::open(&db_path).unwrap();

        // 2. Tweak settings.
        let res : String = dbconn.query_row(
            "PRAGMA journal_mode = WAL",
            NO_PARAMS, |row| row.get(0)).unwrap();
        trace!("sqlite journal_mode = {}", res);
        dbconn.execute_batch("PRAGMA synchronous = NORMAL").unwrap();
        dbconn.execute_batch("PRAGMA foreign_keys = ON").unwrap();
        let fk_enabled : i64 = dbconn.query_row(
            "PRAGMA foreign_keys",
            NO_PARAMS, |row| row.get(0)).unwrap();
        assert_eq!(fk_enabled, 1,
                   "sqlite must be compiled with foreign key support");

        // 3. CREATE TABLES

        // TABLE previous_ids -- Stores the starting id at each execution of
        // the program.  This id is used in all tables as the id column and
        // incremented after every use.
        dbconn.execute_batch(
            "CREATE TABLE IF NOT EXISTS previous_ids (
                 id        INTEGER PRIMARY KEY,
                 firstused BIGINT)",
            ).unwrap();

        // TABLE hashes.
        // Binds the content of a file to an integer id.
        // We enforce the uniqueness on many fields (sha384, filesize) just to
        // be sure there can not ever be duplicates.
        dbconn.execute_batch(
            "CREATE TABLE IF NOT EXISTS hashes (
                 id          INTEGER PRIMARY KEY,
                 firstseen   BIGINT,
                 -- lastseen    BIGINT,  -- ignored for now to avoid constant writes
                 filesize    BIGINT   NOT NULL,
                 sha384      BLOB(48) NOT NULL,
                     UNIQUE (filesize, sha384))",
            ).unwrap();

        // TABLE filenames.
        // In a directory, there are filenames, and each one has an mtime.
        dbconn.execute_batch(
            "CREATE TABLE IF NOT EXISTS filenames (
                 id        INTEGER PRIMARY KEY,
                 firstseen BIGINT,
                 filename  TEXT,
                 mtime     BIGINT)",
            ).unwrap();

        // TABLE trees.
        // A tree is a hierarchy of directories.
        dbconn.execute_batch(
            "CREATE TABLE IF NOT EXISTS trees (
                 id              INTEGER PRIMARY KEY,
                 firstseen       BIGINT,
                 dirname         TEXT      -- for now this is the full path
                 -- parent_tree_id  INTEGER,  -- can be NULL for root nodes otherwise points to root (for now)
                 --     FOREIGN KEY (parent_tree_id) REFERENCES trees(id)
             )",
            ).unwrap();

        // TABLE filesystems.
        //
        // identifying-info: contains a JSON-serialized hashmap, with info
        // that uniquely identifies a filesystem on a device and that we don't
        // store in other columns separately.
        // TODO secondary "info" field with everything else
        // TODO also insert mountpoint, purely for info?
        dbconn.execute_batch(
            "CREATE TABLE IF NOT EXISTS filesystems (
                 id              INTEGER PRIMARY KEY,
                 firstseen       BIGINT,
                 mountpoint_tree_id INTEGER, -- id of mountpoint, or id of file hash if it's an archive
              -- lastscanned      BIGINT,
                 uuid             TEXT, -- filesystem's UUID
                 identifying_info TEXT  -- JSON-serialized data that identifies the filesystem on the specific device
             )",
            ).unwrap();

        // TABLE files_in_trees (aka rel_filenames_containers): N-to-M relations.
        // Many files usually exist in one container (filesystem or archive
        // file), and one file might be contained in many containers.  This
        // table is WITHOUT ROWID and with PRIMARY KEY specifically in order
        // to search fast on one direction (find hash of file), and to ensure
        // no duplicate pairs.
        // TODO consider adding the mtime to this table instead of filenames table.
        dbconn.execute_batch(
            "CREATE TABLE IF NOT EXISTS files_in_trees (
                 fs_id           INTEGER,
                 tree_id         INTEGER,
                 filename_id     INTEGER,
                 hash_id         INTEGER,  -- TODO maybe have hash_id relations in separate table?
                     PRIMARY KEY (fs_id, tree_id, filename_id, hash_id),
                     FOREIGN KEY (fs_id)       REFERENCES filesystems(id),
                     FOREIGN KEY (tree_id)     REFERENCES trees(id),
                     FOREIGN KEY (filename_id) REFERENCES filenames(id),
                     FOREIGN KEY (hash_id)     REFERENCES hashes(id))
             WITHOUT ROWID",
            ).unwrap();


        // 4. Initialize the other fields of the struct Db.

        let last_id : i64 = find_max_id(&dbconn)
            .expect("find_max_id: Could not read from the database")
            .unwrap_or(-1);

        return Ok(Db {
                     dbconn : dbconn,
                    last_id : last_id,
            written_last_id : false,
        });
    }

    // Return the next_id to use in an INSERT query. This means we are writing
    // to the db using this id, so add an entry to previous_ids table, if not
    // done already.
    // TODO: Here we might INSERT the number into previous_ids table, even
    //       though the insertion that follows might fail, for example because
    //       of CONSTRAINT violation.
    fn next_id(&mut self) -> i64
    {
        self.last_id += 1;

        // We only want non-negative ids written to any table.
        assert!(self.last_id >= 0);

        // Insert "id" to "previous_ids" table, if not already inserted.
        if ! self.written_last_id
        {
            let rows_affected = self.dbconn.execute(
                "INSERT INTO
                 previous_ids (id, firstused)
                 VALUES       (?1, ?2)",
                params![self.last_id, now_in_nanos()]
            ).expect("failed INSERT to previous_ids");
            if rows_affected != 1
            {
                panic!("Unexpected: INSERT affected {} rows!",
                       rows_affected);
            }
            trace!("Inserted {} into previous_ids table", self.last_id);
            self.written_last_id = true;
            // Increase last_id once more, since we just used it.
            self.last_id += 1;
        }

        self.last_id
    }

    // Adds to the db all entries needed to describe a file
    // (fs-directory-filename-hash), skipping entries that are already
    // there. file_data should contain the results from previous SELECT
    // queries indicating what already exists in the db.
    pub fn add_file(&mut self, state : &RunningState,
                    file_data : &FileData, existing_ids : &mut DbIds)
        -> Result<(), Box <dyn Error>>
    {
        // Pregenerate ids that /might/ be needed, since the mutable borrow of
        // `self` for executing the prepared statements disallows us to
        // generate these ids inside the function body.
        let reserve_id_1 = self.next_id();
        let reserve_id_2 = self.next_id();
        let reserve_id_3 = self.next_id();
        let reserve_id_4 = self.next_id();
        let reserve_id_5 = self.next_id();

        let mut insert_into_hashes = self.dbconn.prepare(
            "INSERT INTO
             hashes (id, firstseen, filesize, sha384)
             VALUES (?1, ?2, ?3, ?4)")?;
        let mut insert_into_filenames = self.dbconn.prepare(
            "INSERT INTO
             filenames (id, firstseen, filename, mtime)
             VALUES    (?1, ?2, ?3, ?4)")?;
        let mut insert_into_trees = self.dbconn.prepare(
            "INSERT INTO
             trees  (id, firstseen, dirname)
             VALUES (?1, ?2, ?3)")?;
        let mut insert_into_filesystems = self.dbconn.prepare(
            "INSERT INTO
             filesystems (id, firstseen, mountpoint_tree_id, uuid, identifying_info)
             VALUES      (?1, ?2, ?3, ?4, ?5)")?;
        let mut insert_final_relations = self.dbconn.prepare(
            "INSERT INTO
             files_in_trees (fs_id, tree_id, filename_id, hash_id)
             VALUES (?1, ?2, ?3, ?4)")?;


        // If the fs ID was not found in the database, INSERT it, and update
        // the DbIds struct, so that the caller updates the state.
        if existing_ids.fs_id == None
        {
            let fs_devinfo    = state.devinfos.get(& file_data.st_dev)
                .expect("Filesystem not found in lsblk!");
            let drive_info_serialised = match fs_devinfo.drive_dev_id
            {
                None               => None,
                // TODO debug!("Filesystem does not have a parent device in lsblk!"))
                Some(drive_dev_id) =>
                {
                    let drive_devinfo = state.devinfos.get(& drive_dev_id)
                        .expect("Drive not found in lsblk!");
                    let drive_info = DevIdentifyingInfo {
                        drive_size   :   drive_devinfo.size,
                        drive_vendor : & drive_devinfo.vendor.utf8_warn(),
                        drive_model  : & drive_devinfo.model.utf8_warn(),
                        drive_serial : & drive_devinfo.serial,
                        drive_wwn    : & drive_devinfo.wwn,
                    };
                    Some(serde_json::to_string(&drive_info)?)
                }
            };
            existing_ids.fs_id = Some(insert_id(
                &mut insert_into_filesystems, reserve_id_2,
                params![reserve_id_2, now_in_nanos(),
                        Null,
                        fs_devinfo.fs_uuid,
                        drive_info_serialised]));
        }
        let filesystem_id = existing_ids.fs_id.unwrap();

        // let mountpoint_tree_id = existing_ids.mountpoint_tree_id
        //     .unwrap_or_else(
        //     | | insert_id(&mut insert_into_trees, reserve_id_1,
        //                   params![reserve_id_1, now_in_nanos(),
        //                           file_data.fs.mountpoint.to_str().unwrap()]));

        // Either use the existing_ids, or if they are None, insert new
        // entries in the db and use these new ids.

        let tree_id = existing_ids.tree_id
            .unwrap_or_else(
            | | insert_id(&mut insert_into_trees, reserve_id_3,
                          params![reserve_id_3, now_in_nanos(),
                                  file_data.dirname]));
        let filename_id = existing_ids.filename_id
            .unwrap_or_else(
            | | insert_id(&mut insert_into_filenames, reserve_id_4,
                          params![reserve_id_4, now_in_nanos(),
                                  file_data.basename, file_data.mtime]));
        let hash_id = existing_ids.hash_id
            .unwrap_or_else(
            | | insert_id(&mut insert_into_hashes, reserve_id_5,
                          params![reserve_id_5, now_in_nanos(),
                                  file_data.size,
                                  file_data.sha384.as_slice()]));

        if ! existing_ids.final_relation_found
        {
            let rows_affected = insert_final_relations.execute(
                params![filesystem_id, tree_id, filename_id, hash_id])
                .expect("failed INSERT final relation");
            assert!(rows_affected == 1);
        }
        else
        {
            // final_relations_found can only be true if all ids were found.
            assert!(existing_ids.fs_id             .is_some());
            assert!(existing_ids.tree_id           .is_some());
            assert!(existing_ids.filename_id       .is_some());
            assert!(existing_ids.hash_id           .is_some());
        }

        return Ok(());
    }

    // Find the filesystem with the specific UUID on the specific drive.
    pub fn lookup_fs(&self, devinfos: &HashMap<u64,DevInfo>, dev_id: &u64)
        -> Result<Option<i64>, Box<dyn Error>>
    {
        let mut select_fs_id   = self.dbconn.prepare(
              "SELECT id from filesystems WHERE uuid = ?1")?;   // TODO  AND identifying_info = ?2

        // TODO unwrap refactor

        let fs_devinfo = devinfos.get(dev_id).unwrap();

        let fs_id = select_id(&mut select_fs_id,
                              params![fs_devinfo.fs_uuid]);
        if fs_id.is_some()
        {
            debug!("Entering filesystem with UUID '{}' \
                    found in the database with ID {}",
                   fs_devinfo.fs_uuid, fs_id.unwrap());
        }
        else
        {
            debug!("Entering filesystem with UUID '{}' \
                    not found in the database",
                   fs_devinfo.fs_uuid);
        }

        return Ok(fs_id);
    }

    // Lookup details of the file in the database.
    // The fs_id is not looked up in the database, but taken from state.
    // TODO the same with the tree_id.
    pub fn lookup_file(&self, state: &RunningState, file: &FileData)
        -> Result<DbIds, Box<dyn Error>>
    {
        let mut select_tree_id = self.dbconn.prepare(
            "SELECT id FROM trees WHERE dirname = ?")?;
        let mut select_filename_id = self.dbconn.prepare(
            "SELECT id FROM filenames
             WHERE filename = ?1 AND mtime = ?2")?;
        let mut select_hash_id = self.dbconn.prepare(
            "SELECT id FROM hashes
             WHERE filesize = ?1 AND sha384 = ?2")?;
        let mut select_final   = self.dbconn.prepare(
            "SELECT  1  FROM files_in_trees
             WHERE fs_id = ?1 AND tree_id = ?2 AND filename_id = ?3 AND hash_id = ?4")?;
        let mut search_for_corruption = self.dbconn.prepare(
            "SELECT hash_id FROM files_in_trees
             WHERE fs_id = ?1 AND tree_id = ?2 AND filename_id = ?3")?;

        let tree_id =
            select_id(&mut select_tree_id, params![file.dirname]);

        // Search for the filename with the same mtime.
        // TODO what about cleaning up old stale mtime entries?
        let filename_id =
            select_id(&mut select_filename_id,
                      params![file.basename, file.mtime]);

        let hash_id =
            select_id(&mut select_hash_id,
                      params![file.size, file.sha384.as_slice()]);

        let mut final_relation_found : bool        = false;
        let mut different_hash_id    : Option<i64> = None;

        if let (Some(h), Some(f),     Some(t), Some(fs))
             = (hash_id, filename_id, tree_id, state.db_fs_id)
        {
            final_relation_found = select_final.exists(params![fs, t, f, h])
                .expect("failed SELECT final relation");

            if ! final_relation_found
            {
                // Search for different hashes of the exact same
                // file, which should be an indication to CORRUPTION!
                different_hash_id = select_id(&mut search_for_corruption,
                                              params![fs, t, f]);
            }
        }

        return Ok(DbIds {
            fs_id       : state.db_fs_id,
            tree_id     : tree_id,
            filename_id : filename_id,
            hash_id     : hash_id,
            final_relation_found : final_relation_found,
            different_hash_id    : different_hash_id,
        });
    }

    pub fn get_hash_details(&self, hash_id: i64)
        -> Result<Option<HashDetails>, rusqlite::Error>
    {
        let mut select_hash_details = self.dbconn.prepare(
            "SELECT firstseen, filesize, sha384 FROM hashes
             WHERE id = ?")?;
        let result =
            select_hash_details.query_row(params![hash_id],
                |row| Ok(HashDetails {
                    firstseen : row.get(0)?,
                    filesize  : row.get::<_,Option<_>>(1)?
                                .expect("No NULL values allowed in db field"),
                    sha384    : row.get::<_,Option<Vec<u8>>>(2)?
                                .expect("No NULL values allowed in db field"),
                })
            ).optional();

        // Assert database BLOB is of correct size
        if let Ok(Some(hd)) = &result    { assert_eq!(hd.sha384.len(), 48) }
        result
    }
}
#[derive (Debug)]
pub struct HashDetails
{
    firstseen : Option<i64>,
    filesize  : i64,
    sha384    : Vec<u8> // [u8; 48] needs impl FromSql
}
/*
// Needs own type that wraps [u8; 48]. Maybe GenericArray can help?
impl rusqlite::types::FromSql for [u8; 48]
{
    fn column_result(value: rusqlite::types::ValueRef)
        -> rusqlite::types::FromSqlResult<Self>
    {
    }
}
*/

fn now_in_nanos() -> i64
{
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH).unwrap()
        .as_nanos() as i64
}

// Scan all tables and find the max_id.
// Return None if there are no entries anywhere.
fn find_max_id(dbconn : &rusqlite::Connection)
               -> Result<Option<i64>, Box<dyn Error>>
{
    let mut max_ids: Vec<i64> = Vec::new();
    for t in TABLES_WITH_ID.iter()
    {
        let maybe_id : Option<i64> = dbconn.query_row(
            &format!("SELECT max(id) FROM {}", &t),
            NO_PARAMS, |row| row.get(0)).unwrap();
        trace!("max id from table  {:12} : {:?}",
               t, maybe_id);
        if let Some(n) = maybe_id
        {
            assert!(!max_ids.contains(&n),
                    "All ids in db have to be different");
            max_ids.push(n);
        }
    }
    let max_id : Option<i64> = max_ids.into_iter().max();
    debug!("Last used id in db: {:?}", max_id);

    return Ok(max_id);
}

// TODO assert only zero on one row is found.
fn select_id<P>(stmt: &mut rusqlite::Statement,
                params: P)
    -> Option<i64>
where
    P       : IntoIterator,
    P::Item : rusqlite::ToSql,
{
    // Each `query_row().optional()?` returns an Option<Option<i64>>. The
    // outer Option checks whether a row was found, the inner option
    // checks whether the value found was NULL. The latter should never
    // happen, thus we unwrap it (with expect()) when we read the row.
    stmt.query_row(
        params,
        |row| Ok(row.get::<_,Option<i64>>(0)
                     .expect("SQLite error getting row")
                     .expect("No NULL ID values should exist in the table"))
    ).optional().expect("SQLite failed running SELECT query")
}

// TODO FIX id is passed twice for now, once as its own parameter,
//      and once in params[0].
fn insert_id<P>(stmt: &mut rusqlite::Statement,
                id: i64, params: P)
    -> i64
where
    P       : IntoIterator,
    P::Item : rusqlite::ToSql,
{
    let rows_affected = stmt.execute(params)
        .expect("SQLITE failed running INSERT query");
    // TODO what if it failed because of constraint violation?
    assert!(rows_affected == 1);
    id
}
