//#![allow(unused_variables)]
//#![allow(dead_code)]

pub mod config;
use config::Config;
pub mod running_state;
use running_state::RunningState;
mod db;
use db::Db;

use std::convert::TryFrom;
use std::error::Error;
use std::fs;
use std::io;
use std::path::{Path,PathBuf};
use std::time::SystemTime;

use std::os::linux::fs::MetadataExt;

use walkdir::WalkDir;
use sha2::Sha384;

// Workaround version mismatch of digest by using the version from sha2.
//use digest::Digest;
use sha2::digest::generic_array::GenericArray;
use sha2::Digest;

#[allow(unused_imports)]
use log::{error,warn,info,debug,trace,log_enabled};             // macro



pub struct FileData
{
    orig_path : PathBuf,
    // TODO the following fields should be &Path referencing the orig_path PathBuf
    path     : PathBuf,                          // mountpoint prefix stripped
    basename : String,
    dirname  : String,
    mtime  : i64,
    sha384 : GenericArray<u8, <Sha384 as Digest>::OutputSize>,
    size   : i64,
    // This can help us to avoid traversing filesystem boundaries, by
    // comparing it to current file system from struct state.
    st_dev : u64,
    // TODO see if this file has multiple hard-links pointing to it, and if so
    //      add it to a hash table of inodes.
    //st_nlink : u64,
    //st_ino   : u64,
}

pub fn run<'a>(mut state: &mut RunningState,
                  config: &Config)
    -> Result<(), Box<dyn Error>>
{
    if ! config.data_dir.is_dir()
    {
        info!("Creating data directory: {}", config.data_dir.display());
        fs::create_dir(&config.data_dir)?;
    }

    info!("Opening database: {}", config.db_file.display());
    let mut db = Db::new(&config.db_file)?;
    // TODO option to open the database read-only

    if ! fs::symlink_metadata(&config.target_path)?.is_dir()
    {
        error!("For now we only accept directory as argument");
        // let dev_id_of_file = fs::symlink_metadata(&config.target_path)?.st_dev();
        // let mountpoint = state.get_mountpoint_of_device(dev_id_of_root);
        // ...continue with processing only this single file
        std::process::exit(1);
    }

    for entry in WalkDir::new(&config.target_path)
    {
        if let Err(e) = entry
        {
            // Skip if we can't descend, for example because of PermissionDenied
            warn!("Skipping because of error: {}", e);
            continue;
        }
        let entry = entry.unwrap();
        let entry_type = entry.file_type();

        if entry_type.is_dir()
        {
            trace!("Entering new directory: {}",
                   entry.path().display());
            // Detect the device id (major:minor numbers) of the directory
            // we'll recurse into (directories come first during WalkDir and
            // then come their contents). We use this later to strip the
            // mountpoint prefix from the path of each file.

            // TODO BUG sometimes we return up from a directory to continue
            //          with more files in the parent!

            // TODO we have to remember which filesystems (st_devs) have been
            // in the db while we are traversing, to avoid rewritting them.

            let dev_id : u64 = entry.metadata()?.st_dev();
            if state.dev_id != Some(dev_id)
            {
                state.dev_id     = Some(dev_id);
                state.dev_fs_idx = state.get_idx_of_fs(dev_id);
                state.db_fs_id   = db.lookup_fs(&state.devinfos, &dev_id)?;

                // TODO if this is a different filesystem, then skip it, and
                //      add it to a list to recurse into it later.

                // We are changing filesystem, we need to look up
                // the fs_id in the database again.
                // if state.db_fs_id.is_some()
                // {
                //     state.db_fs_id = db.lookup_fs(&state, &dev_id)?;
                // }
            }
        }
        // Calculate checksums and write them to the database.
        // Skip symlinks, special files, etc.
        else if entry_type.is_file()
        {
            let   maybe_file = fs::File::open(&entry.path());
            match maybe_file
            {
            Err(e)   => warn!("Skipping file: {}  (open: {})",
                              &entry.path().display(), e),
            Ok(file) =>
            {
                let   maybe_filedata = process_file(&state, file, entry.path());
                match maybe_filedata
                {
                Err(err) => warn!("Skipping file: {}  (error: {})",
                                  entry.path().display(), err),
                Ok(file_data) =>
                {
                    // Check what parts of the file (filename, dirname,
                    // fs, hash) can be found in the db.
                    // "fs" is not searched in db, but taken from state.
                    let mut ids = db.lookup_file(&state, &file_data)?;

                    if ids.final_relation_found
                    {
                        debug!("File found and verified: {}",
                               file_data.basename);
                    }
                    // If it was not found in the relations table, we
                    // searched the table without specifying the hash_id.
                    else if let Some(hash2_id) = ids.different_hash_id
                    {
                        println!("WARNING: corruption detected for file: {}",
                                 file_data.basename);
                        let hash_details = db.get_hash_details(hash2_id)?;
                        println!("    hash_details: {:?}", hash_details);
                    }
                    // Either the final relation was not found, or other
                    // parts if the file. Anyway, insert whatever missing.
                    else
                    {
                        println!("Adding file: {}",
                                 file_data.basename);
                        db.add_file(&state, &file_data, &mut ids)?;

                        // Update state, if a new id was INSERTed in the db.
                        if ids.fs_id != state.db_fs_id
                        {
                            assert!(state.db_fs_id.is_none());
                            state.db_fs_id = ids.fs_id;
                        }
                    }
                },
                }
            },
            }
        }
        else
        {
            debug!("Skipping non-regular file: {}",
                   &entry.path().display());
        }
    }

    Ok(())
}


// Return the number of nanoseconds since the epoch as i64.
// Can be negative if the timestamp is before the epoch. Returns error in case
// of overflow.
fn timestamp_portable(t : SystemTime) -> Result<i64, &'static str>
{
    match t.duration_since(SystemTime::UNIX_EPOCH)
    {
        Ok(d)  => i64::try_from(d.as_nanos())
            .or(Err("timestamp too far in the future")),
        // t is before the epoch, so we have to calculate it negative
        Err(_) =>
        {
            match i64::try_from(SystemTime::UNIX_EPOCH
                                .duration_since(t).unwrap().as_nanos())
            {
                // Any positive number shouldn't overflow when negating
                Ok(d)  => Ok(-d),
                Err(_) => Err("timestamp too far in the past"),
            }
        },
    }
}

fn process_file<'a>(state: &'a RunningState,
                    mut file: fs::File,
                    path: &Path)
    ->  Result<FileData, Box<dyn Error>>
{
    let (filesize,hash) = hash_file(&mut file).unwrap();

    let metadata = file.metadata()?;
    let mtime : SystemTime = metadata.modified()
        .expect("FATAL: system or filesystem does not support mtime of files; \
                 this is required to distinguish between files changed on \
                 purpose and corrupted files");

    let mtime_portable : i64 = timestamp_portable(mtime)?;

    let st_dev : u64 = metadata.st_dev();

    // We can't jump to a different filesystem just by reading a file, we have
    // to enter a directory or follow a symlink at least.
    assert!(st_dev == state.dev_id.unwrap());

    return Ok(FileData {
        orig_path : path.to_path_buf(),
        path      : path.strip_prefix(
            &state.mounts[state.dev_fs_idx.unwrap()].mountpoint)
            .unwrap().to_path_buf(),
        basename : path.file_name().unwrap().to_str().unwrap().to_string(),
        dirname  : path.parent()   .unwrap().to_str().unwrap().to_string(),
        mtime  : mtime_portable,
        sha384 : hash,
        size   : filesize as i64,
        st_dev : st_dev,
    })
}


fn hash_file(mut file: impl io::Read)
    ->  Result<(u64, GenericArray<u8, <Sha384 as Digest>::OutputSize>),
               io::Error>
{
    let mut hasher = Sha384::new();
    let n = std::io::copy(&mut file, &mut hasher)?;
    let hash = hasher.result();

    return Ok((n,hash));
}
