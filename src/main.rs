use scrubber::config::Config;
use scrubber::running_state;
use scrubber::running_state::RunningState;


use std::env;
use std::error::Error;
use std::ffi::OsString;

#[allow(unused_imports)]
use log::{error,warn,info,debug,trace,log_enabled};             // macro


static PROGNAME : &'static str = "scrubber";


fn main() -> Result<(), Box<dyn Error>>
{
    let mut args: Vec<OsString> = env::args_os().collect();

    let config = Config::new(&PROGNAME, &mut args)?;

    let mut state = RunningState {
        mounts   : running_state::get_all_mountpoints()?,
        devinfos : running_state::lsblk()?,
        dev_id         : None,
        dev_fs_idx     : None,
        db_fs_id       : None,
    };

    scrubber::run(&mut state, &config)
}
