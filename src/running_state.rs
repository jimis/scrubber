use std::collections::HashMap;
use std::error::Error;
use std::ffi::{OsStr,OsString};
use std::io::BufRead;
use std::os::unix::ffi::OsStrExt;                         // for parsing lsblk
use std::path::{Path,PathBuf};
use std::process::{Command,Output};
use std::str;

#[allow(unused_imports)]
use log::{error,warn,info,debug,trace,log_enabled};             // macro


pub struct RunningState
{
    // List of all mounts in the system.
    pub mounts  : Vec<FsInfo>,
    // Details about each device, indexed by the st_dev u64 number.
    pub devinfos : HashMap<u64,DevInfo>,
    // Id (major:minor) and mountpoint of the device, that we read currently
    // files from. Used for stripping the mountpoint prefix from the path.
    pub dev_id         : Option<u64>,
    // TODO problems with lifetimes: how can this &FsInfo point inside "mounts" above?
    //    pub dev_fsinfo     : Option<&FsInfo>,
    // Workaround is to store the index of the entry in the Vec.
    pub dev_fs_idx : Option<usize>,
    // ID of the current filesystem in the database.
    pub db_fs_id   : Option<i64>,
}

pub struct FsInfo
{
    pub major      : u32,
    pub minor      : u32,
    pub st_dev     : u64,
    pub mountpoint : PathBuf,
}

fn parse_major_minor(maj_min: &str)
    -> Result<(u32,u32, u64), &'static str>
{
    let mut fields = maj_min.split(":");
    let major = fields.next()
        .ok_or("empty 'major:minor' field")?
        .parse()
        .or(Err("error parsing 'major' device number"))?;
    let minor = fields.next()
        .ok_or("no colon found in 'major:minor' field")?
        .parse()
        .or(Err("error parsing 'minor' device number"))?;
    if ! fields.next().is_none()
    {
        return Err("more than one colon found in 'major:minor' field");
    }
    let dev_id = unsafe { libc::makedev(major, minor) };

    return Ok((major, minor, dev_id));
}

// PLATFORM-SPECIFIC
// For now it just parses /proc/self/mountinfo.
// https://www.kernel.org/doc/Documentation/filesystems/proc.txt
pub fn get_all_mountpoints()
    -> Result<Vec<FsInfo>, Box<dyn Error>>
{
    let mut result : Vec<FsInfo> = Vec::new();

    let f = std::fs::File::open("/proc/self/mountinfo")
        .expect("FATAL: Could not open /proc/self/mountinfo");
    for l in std::io::BufReader::new(f).lines()
    {
        let line = l?;
        let fields : Vec<&str> = line.split_whitespace().collect();

        let (major, minor, st_dev) = parse_major_minor(&fields[2])?;

        result.push(FsInfo {
            major      : major,
            minor      : minor,
            st_dev     : st_dev,
            mountpoint : Path::new(fields[4]).to_path_buf(),
        });
    }
    result.shrink_to_fit();
    return Ok(result);
}

// TODO check if all these fields are available in lsblk from RHEL 6 or older.
//      If they are not, I'll have to parse --help to decide which ones to
//      request, and then store only what is found in a hash table.
#[allow(dead_code)]
pub struct DevInfo
{
    pub       dev_id : u64,
    // This is the dev_id of the "parent" device,
    // for example /dev/sda for partition /dev/sda1
    pub drive_dev_id : Option<u64>,

    pub fs_uuid    : String,
    pub part_uuid  : String,
    pub name       : OsString,
    pub kname      : OsString,
    pub pkname     : OsString,

    pub mountpoint : OsString,
    pub dev_major  : u32,
    pub dev_minor  : u32,
    pub fs_label   : OsString,
    pub part_label : OsString,
    pub model      : OsString,
    pub serial     : String,
    pub size       : u64,
    pub state      : String,
    pub typ        : String,
    pub wwn        : String,
    pub vendor     : OsString,
}

// TODO strip all whitespace from text fields
pub fn lsblk()
    -> Result<HashMap<u64,DevInfo>, Box<dyn Error>>
{
    let fields = "NAME,KNAME,PKNAME,MOUNTPOINT,MAJ:MIN,UUID,LABEL,PARTLABEL,\
                  PARTUUID,MODEL,SERIAL,SIZE,STATE,TYPE,WWN,VENDOR";

    // TODO TEST somehow provide custom process::Output here for testing.
    let mut lsblk = Command::new("lsblk");
    lsblk.arg("-r")                                          // format: raw
         .arg("-b")                                          // sizes in bytes
         .arg("-o").arg(fields);
    debug!("Executing command: {:?}", lsblk);
    let lsblk_output : Output = lsblk.output()?;
    if ! lsblk_output.status.success()
    {
        return Err(format!("lsblk exited with {:?}, stderr: \n{}",
                           lsblk_output.status.code(),
                           String::from_utf8_lossy(&lsblk_output.stderr))
                   .into());
    }

    let mut lines_iter = lsblk_output.stdout.split(|&c| c == b'\n');

    let header_line : &[u8] = lines_iter.next()
        .expect("lsblk output missing header line");

    let headers : Vec<&str> = header_line
        .split(|&c| c == b' ')
        .map(|s| str::from_utf8(s)
             .expect("lsblk header line should be plain ASCII"))
        .collect();

    // HashMap from dev_id to DevInfo
    let mut lsblk_map : HashMap<u64,DevInfo> = HashMap::new();

    for l in lines_iter
    {
        // Early return if this is the last line, because the upcoming
        // split(' ') on an empty string returns an empty string!
        if l.len() == 0  { continue; }

        let fields : Vec<&[u8]> = l.split(|&c| c == b' ').collect();
        if  fields.len() != headers.len()
        {
            warn!("lsblk returned a line with {} elements, \
                   instead of {} of the header, ignoring line: \n'{}'",
                  fields.len(), headers.len(),
                  String::from_utf8_lossy(l));
            continue;
        }

        let maj_min =
            text_field_from_lsblk_line("MAJ:MIN", &fields, &headers)?;

        let (dev_major, dev_minor, dev_id) = parse_major_minor(&maj_min)?;

        let fs_uuid   = text_field_from_lsblk_line("UUID",
                                                   &fields, &headers)?;
        let part_uuid = text_field_from_lsblk_line("PARTUUID",
                                                   &fields, &headers)?;
        let serial = text_field_from_lsblk_line("SERIAL",
                                                &fields, &headers)?;
        let state = text_field_from_lsblk_line("STATE", &fields, &headers)?;
        let typ   = text_field_from_lsblk_line("TYPE", &fields, &headers)?;
        let wwn   = text_field_from_lsblk_line("WWN", &fields, &headers)?;

        let size : u64 =
            text_field_from_lsblk_line("SIZE", &fields, &headers)?
            .parse().or(Err("error parsing SIZE field in lsblk"))?;

        // lsblk -r outputs in raw format: one space separates each field, and in
        // field spaces and other special characters are hex-escaped \xFF.

        let name   = decode_field_from_lsblk_line("NAME",   &fields, &headers)?;
        let kname  = decode_field_from_lsblk_line("KNAME",  &fields, &headers)?;
        let pkname = decode_field_from_lsblk_line("PKNAME", &fields, &headers)?;
        let mountpoint =
            decode_field_from_lsblk_line("MOUNTPOINT", &fields, &headers)?;
        let fs_label   = decode_field_from_lsblk_line("LABEL",
                                                      &fields, &headers)?;
        let part_label =
            decode_field_from_lsblk_line("PARTLABEL", &fields, &headers)?;
        let model =
            decode_field_from_lsblk_line("MODEL", &fields, &headers)?;
        let vendor =
            decode_field_from_lsblk_line("VENDOR", &fields, &headers)?;
        // Will detect the parent device after all lines have been parsed.
        let drive_dev_id = None;

        let devinfo =
            DevInfo { dev_major,dev_minor,dev_id,fs_uuid,part_uuid,serial,
                      state,typ,wwn,size,name,kname,pkname,mountpoint,
                      fs_label,part_label,model,vendor,drive_dev_id };

        // Store all fields of the line in a hash table with key the dev_id.
        if let Some(old_devinfo) = lsblk_map.insert(dev_id, devinfo)
        {
            warn!("Duplicate device 'MAJ:MIN' numbers! \
                   Old:'{}' replaced with new:'{}'",
                  old_devinfo.kname.to_string_lossy(),
                  lsblk_map.get(&dev_id).unwrap().kname.to_string_lossy());
        }
    }

    // Find the parent device for all devices, store them in a vector, and
    // then modify all entries in the HashMap.
    let drive_ids = compute_drive_ids(&lsblk_map);
    for (k,v) in drive_ids
    {
        // TODO assert that k is found in lsblk_map.
        lsblk_map.entry(k).and_modify(
            |e| e.drive_dev_id = Some(v));
    }

    lsblk_map.shrink_to_fit();
    Ok(lsblk_map)
}

// Return a list of pairs of (dev_id, parent_dev_id)
fn compute_drive_ids(lsblk_map: &HashMap<u64,DevInfo>) -> Vec<(u64,u64)>
{
    let mut vec = Vec::new();
    for key in lsblk_map.keys()
    {
        let parent_dev_id = find_parent_dev_id(&lsblk_map, key);
        if *key != parent_dev_id
        {
            vec.push( (*key, parent_dev_id) );
        }
    }
    return vec;
}

// Recursively finds the parent device of a device until a device that has no
// parent.  Returns the same dev_id if the device has no 'PKNAME'.
fn find_parent_dev_id(lsblk_map: &HashMap<u64,DevInfo>, dev_id: &u64)
    -> u64
{
    let pkname = & lsblk_map.get(dev_id).unwrap().pkname;
    if pkname == ""
    {
        return *dev_id;
    }
    let parent_dev_id = lsblk_map.iter()
        .find( |(k,v)| {
                   assert_eq!(*k, &v.dev_id);
                   &v.kname == pkname })
        .expect("lsblk line has a line with 'PKNAME' \
                 that is not found elsewhere as 'KNAME'")
        .0;

    find_parent_dev_id(lsblk_map, &parent_dev_id)
}

fn text_field_from_lsblk_line(
    fieldname: &str, line: &Vec<&[u8]>, headers: &Vec<&str>)
    -> Result<String, &'static str>
{
    Ok(str::from_utf8
       (&line[headers.iter().position(|&s| s == fieldname)
                .ok_or("did not find field in lsblk header")?]
       ).or(Err("field was not encoded properly"))?
       .to_owned())
}

fn decode_field_from_lsblk_line(
    fieldname: &str, line: &Vec<&[u8]>, headers: &Vec<&str>)
    -> Result<OsString, &'static str>
{
    Ok(OsStr::from_bytes
       (&hex_decode(
            &line[headers.iter().position(|&s| s == fieldname)
                  .ok_or("Did not find field in lsblk header")?] )?
       ).to_owned())
}

fn hex_decode(s: &[u8])
    -> Result<Vec<u8>, &'static str>
{
    let mut res : Vec<u8> = Vec::with_capacity(s.len());
    let mut i = 0;
    while i < s.len()
    {
        if  s[i]   == b'\\' && i+1 < s.len() &&
            s[i+1] == b'x'
        {
            if i+3 < s.len()
            {
                let value : u8 = hexdigit(s[i+2])? * 16 +
                                 hexdigit(s[i+3])?;
                res.push(value);
                i += 4
            }
            else
            {
                return Err("While hex-decoding, \\x was not followed by two hex digits");
            }
        }
        else
        {
            res.push(s[i]);
            i += 1;
        }
    }
    res.shrink_to_fit();
    return Ok(res);
}

fn hexdigit(c: u8) -> Result<u8, &'static str>
{
    match c
    {
        b'0' ..= b'9' =>  Ok(c - b'0'),
        b'A' ..= b'F' =>  Ok(c - b'A' + 10),
        b'a' ..= b'f' =>  Ok(c - b'a' + 10),
        _ => return Err("Character was not a hex digit"),
    }
}

impl RunningState
{
    // TODO MAYBE an alternative way is to stat() parent directories until
    //     st_dev changes or we hit /. Does not work with bind mounts.
    pub fn get_mountpoint_of_device(&self, st_dev: u64)
        -> Result<&Path, Box<dyn Error>>
    {
        // TODO what if there are multiple entries with same st_dev?
        let fsinfo = self.mounts.iter()
            .filter(|fsinfo| fsinfo.st_dev == st_dev)
            .next().expect("device number {} not found mounted anywhere!");
        return Ok(&fsinfo.mountpoint);
    }
    pub fn get_fs_of_device(&self, st_dev: u64)
        -> Result<&FsInfo, Box<dyn Error>>
    {
        // TODO what if there are multiple entries with same st_dev?
        let fsinfo = self.mounts.iter()
            .filter(|fsinfo| fsinfo.st_dev == st_dev)
            .next().expect("device number {} not found mounted anywhere!");
        return Ok(&fsinfo);
    }
    pub fn get_idx_of_fs(&self, st_dev: u64)
        -> Option<usize>
    {
        self.mounts.iter().position(|fsinfo| fsinfo.st_dev == st_dev)
    }
}


#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_parse_major_minor()
    {
        assert_eq!(parse_major_minor("0:22"),
                   Ok((0, 22, unsafe { libc::makedev(0, 22) } )));
        assert_eq!(parse_major_minor("259:6"),
                   Ok((259, 6, unsafe { libc::makedev(259, 6) } )));
        assert_eq!(parse_major_minor("0:0"),
                   Ok((0, 0, unsafe { libc::makedev(0, 0) } )));

        assert!(parse_major_minor(" 0:0").is_err());
        assert!(parse_major_minor("0:0 ").is_err());
        assert!(parse_major_minor(" 0:0 ").is_err());

        assert!(parse_major_minor("0").is_err());
        assert!(parse_major_minor("").is_err());
        assert!(parse_major_minor("a").is_err());
        assert!(parse_major_minor("0:").is_err());
        assert!(parse_major_minor("a:b").is_err());
        assert!(parse_major_minor(":0").is_err());
        assert!(parse_major_minor("a:0").is_err());
        assert!(parse_major_minor("0:0:").is_err());
        assert!(parse_major_minor("0:0:0").is_err());
    }

    #[test]
    fn test_hexdigit()
    {
        assert_eq!(hexdigit(b'0'), Ok(0));
        assert_eq!(hexdigit(b'1'), Ok(1));
        assert_eq!(hexdigit(b'9'), Ok(9));
        assert_eq!(hexdigit(b'A'), Ok(10));
        assert_eq!(hexdigit(b'F'), Ok(15));
        assert_eq!(hexdigit(b'a'), Ok(10));
        assert_eq!(hexdigit(b'f'), Ok(15));

        assert!(hexdigit(b' ').is_err());
    }

    #[test]
    fn test_hex_decode()
    {
        assert_eq!(hex_decode("".as_bytes()),
                           Ok("".as_bytes().to_vec()));
        assert_eq!(hex_decode("abc".as_bytes()),
                           Ok("abc".as_bytes().to_vec()));
        assert_eq!(hex_decode("x".as_bytes()),
                           Ok("x".as_bytes().to_vec()));

        assert_eq!(hex_decode(r"\x20".as_bytes()),
                            Ok(" ".as_bytes().to_vec()));
        assert_eq!(hex_decode(r"\x200".as_bytes()),
                               Ok(" 0".as_bytes().to_vec()));
        assert_eq!(hex_decode(r"0\x20".as_bytes()),
                            Ok("0 ".as_bytes().to_vec()));

        assert!(hex_decode(r"\x2".as_bytes()).is_err());
        assert!(hex_decode(r"\x2g".as_bytes()).is_err());
        assert!(hex_decode(r"\xg".as_bytes()).is_err());
    }
}
